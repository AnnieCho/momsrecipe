package com.project.innovator.momsrecipe.models;

import android.os.Parcel;
import android.os.Parcelable;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class Recipe implements Parcelable {

    private String name;
    private String ingredients;
    private String condiment;
    private String contents;
    private HashMap<String, String> images;

    public Recipe() {

    }

    public Recipe(String name, String ingredients, String condiment, String contents, HashMap<String, String> images) {
        this.name = name;
        this.ingredients = ingredients;
        this.condiment = condiment;
        this.contents = contents;
        this.images = images;
    }

    public Recipe(Parcel parcel){
        this.name = parcel.readString();
        this.ingredients = parcel.readString();
        this.condiment = parcel.readString();
        this.contents = parcel.readString();
        this.images = parcel.readHashMap(Recipe.class.getClassLoader());
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setIngredients(String ingredients) {
        this.ingredients = ingredients;
    }

    public void setCondiment(String condiment) {
        this.condiment = condiment;
    }

    public void setContents(String contents) {
        this.contents = contents;
    }

    public void setImages(HashMap<String, String> images) {
        this.images = images;
    }

    public String getName() {
        return name;
    }

    public String getIngredients() {
        return ingredients;
    }

    public String getCondiment() {
        return condiment;
    }

    public String getContents() {
        return contents;
    }

    public HashMap<String, String> getImages() {
        return images;
    }

    public List<String> getImagePath() {
        if (images != null && !images.isEmpty()) {
            List<String> result = new ArrayList<>();
            for (String path : getImagesKeys()) {
                result.add(images.get(path));
            }
            return result;
        }
        return null;
    }

    public List<String> getImagesKeys() {
        if (images != null && !images.isEmpty()) {
            List<String> result = new ArrayList<>();
            for(String key : images.keySet()) {
                result.add(key);
            }
            return result;
        }
        return null;
    }

    public Map<String, Object> toMap() {
        HashMap<String, Object> result = new HashMap<>();
        result.put("name", name);
        result.put("ingredients", ingredients);
        result.put("condiment", condiment);
        result.put("contents", contents);
        result.put("images", images);
        return result;
    }

    public boolean equals(Recipe recipe) {
        if(this.name.equals(recipe.getName())
            && this.ingredients.equals(recipe.getIngredients())
            && this.condiment.equals(recipe.getCondiment())
            && this.contents.equals(recipe.getContents())
            && this.images.equals(recipe.getImages()))
            return true;

        return false;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeString(this.name);
        parcel.writeString(this.ingredients);
        parcel.writeString(this.condiment);
        parcel.writeString(this.contents);
        parcel.writeMap(this.images);
//        if(images.size() > 0){
//            Set<String> keySet = images.keySet();
//            Bundle b = new Bundle();
//            for(String key : keySet){
//                b.putString(key, images.get(key));
//            }
//
//            String[] array = keySet.toArray(new String[keySet.size()]);  //??
//            parcel.writeStringArray(array);
//            parcel.writeBundle(b);
//        }
    }

    public static final Parcelable.Creator<Recipe> CREATOR = new Parcelable.Creator<Recipe>(){
        @Override
        public Recipe createFromParcel(Parcel parcel) {
            return new Recipe(parcel);
        }

        @Override
        public Recipe[] newArray(int i) {
            return new Recipe[i];
        }
    };
}