package com.project.innovator.momsrecipe.views.holder;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.project.innovator.momsrecipe.R;

public class RecommendDialogViewHolder extends RecyclerView.ViewHolder {

    private ImageView titleImageView;
    private TextView nameTextView;

    public RecommendDialogViewHolder(View itemView) {
        super(itemView);

        titleImageView = itemView.findViewById(R.id.dialog_recommend_title);
        nameTextView = itemView.findViewById(R.id.dialog_recommend_name);
    }

    public ImageView getTitleImageView() {
        return titleImageView;
    }

    public TextView getNameTextView() {
        return nameTextView;
    }
}
