package com.project.innovator.momsrecipe.views.dialog;

import android.app.Dialog;
import android.content.Context;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.view.WindowManager;
import android.widget.ImageButton;
import android.widget.LinearLayout;

import com.project.innovator.momsrecipe.R;
import com.project.innovator.momsrecipe.adapter.RecommendDialogAdapter;
import com.project.innovator.momsrecipe.models.Recipe;

import java.util.List;

public class RecommendShowDialog extends Dialog {

    private Context context;
    private RecyclerView.Adapter adapter;
    private List<Recipe> items;


    private RecyclerView recyclerView;
    private ImageButton closeButton;

    public RecommendShowDialog(@NonNull Context context) {
        super(context);
    }

    public RecommendShowDialog(@NonNull Context context, int themeResId) {
        super(context, themeResId);
    }

    protected RecommendShowDialog(@NonNull Context context, boolean cancelable, @Nullable OnCancelListener cancelListener) {
        super(context, cancelable, cancelListener);
    }


    public RecommendShowDialog(Context context, List<Recipe> items) {
        super(context, android.R.style.Theme_Translucent_NoTitleBar);
        this.context = context;
        this.items = items;
    }

    public RecommendShowDialog(Context context, RecyclerView.Adapter adapter) {
        super(context, android.R.style.Theme_Translucent_NoTitleBar);
        this.context = context;
        this.adapter = adapter;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        WindowManager.LayoutParams windowLayoutParams = new WindowManager.LayoutParams();
        windowLayoutParams.flags = WindowManager.LayoutParams.FLAG_DIM_BEHIND;
        windowLayoutParams.dimAmount = 0.8f;
        getWindow().setAttributes(windowLayoutParams);

        setContentView(R.layout.dialog_recommend);

        closeButton = findViewById(R.id.dialog_recommend_closeButton);
        closeButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dismiss();
            }
        });

        recyclerView = findViewById(R.id.dialog_recommend_recyclerView);
        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(context, LinearLayout.VERTICAL, false);
        recyclerView.setLayoutManager(layoutManager);

        if(adapter == null)
            if(items != null && !items.isEmpty())
                adapter = new RecommendDialogAdapter(context, items);

        recyclerView.setAdapter(adapter);
    }
}
