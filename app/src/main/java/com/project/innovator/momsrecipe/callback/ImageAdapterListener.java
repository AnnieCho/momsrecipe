package com.project.innovator.momsrecipe.callback;

public interface ImageAdapterListener {
    void removeImageItem(int position);
}
