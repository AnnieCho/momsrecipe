package com.project.innovator.momsrecipe.controller;

import android.annotation.SuppressLint;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.FloatingActionButton;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.Toast;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DatabaseError;
import com.project.innovator.momsrecipe.FirebaseUtils;
import com.project.innovator.momsrecipe.R;
import com.project.innovator.momsrecipe.models.Material;
import com.project.innovator.momsrecipe.models.Recipe;
import com.project.innovator.momsrecipe.views.RefrigeratorLayout;
import com.project.innovator.momsrecipe.views.dialog.RecommendShowDialog;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

@SuppressLint("ValidFragment")
public class RefrigeratorFragment extends BaseFragment {

    private List<Integer> resources;

    private RefrigeratorLayout refrigeratorLayout;
    private FloatingActionButton floatingMainButton,
        floatingAddButton,
        floatingEditButton,
        floatingRecommendButton;
    private Boolean isFabOpen = false;
    private Animation fab_open, fab_close;

    public RefrigeratorFragment(int page, String title) {
        super(page, title);
        resources = new ArrayList<>();
        resources = new ArrayList<>(Arrays.asList(R.drawable.material0, R.drawable.material1,
            R.drawable.material2, R.drawable.material3, R.drawable.material4, R.drawable.material5, R.drawable.material6, R.drawable.material7,
            R.drawable.material8, R.drawable.material9, R.drawable.material10, R.drawable.material11));
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_refrigerator, null);

        fab_open= AnimationUtils.loadAnimation(getContext(), R.anim.fab_open);
        fab_close=AnimationUtils.loadAnimation(getContext(), R.anim.fab_close);

        refrigeratorLayout = view.findViewById(R.id.refrigerator);
        refrigeratorLayout.setResources(resources);
        floatingMainButton = view.findViewById(R.id.fab);
        floatingMainButton.setImageResource(R.drawable.refrigerator_float);
        floatingMainButton.setOnClickListener(mainButtonClickListener);
        floatingAddButton = view.findViewById(R.id.material_add);
        floatingAddButton.setImageResource(R.drawable.add_food);
        floatingAddButton.setOnClickListener(addButtonClickListener);
        floatingEditButton = view.findViewById(R.id.material_edit);
        floatingEditButton.setImageResource(R.drawable.modify_food_1);
        floatingEditButton.setOnClickListener(editButtonClickListener);
        floatingRecommendButton=view.findViewById(R.id.material_recommend);
        floatingRecommendButton.setOnClickListener(recommendButtonClickListener);
        floatingRecommendButton.setImageResource(R.drawable.recommendbutton);
        return view;
    }

    private  View.OnClickListener mainButtonClickListener= new View.OnClickListener() {
        @Override
        public void onClick(View view) {
            animateFAB();
        }
    };

    //RefrigeratorLayout 에서 추가와 삭제를 하는 구조가 더 깔끔할것 같음
    private View.OnClickListener addButtonClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View view) {
            refrigeratorLayout.editMaterialView();
        }
    };

    private View.OnClickListener editButtonClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View view) {
            if(refrigeratorLayout.switchMaterialEditMode()) {
                floatingEditButton.setImageResource(R.drawable.modify_food_2);
            }
            else {
                floatingEditButton.setImageResource(R.drawable.modify_food_1);
            }
        }
    };

    private View.OnClickListener recommendButtonClickListener = (new View.OnClickListener(){
        @Override
        public void onClick(View view) {
            final FirebaseUser user = FirebaseAuth.getInstance().getCurrentUser();
            FirebaseUtils.getAllMaterial(user.getUid(), new FirebaseUtils.MaterialCallback() {
                @Override
                public void getMaterialDataCallback(List<Material> materials) {
                    if(materials != null && !materials.isEmpty()) {
                        List<String> list = new ArrayList<>();
                        for(Material material : materials) {
                            list.add(material.getName());
                        }
                        FirebaseUtils.getFindRecipe(user.getUid(), list, new FirebaseUtils.RecipeCallback() {
                            @Override
                            public void getRecipeDataCallback(List<Recipe> recipes) {
                                if(recipes != null && !recipes.isEmpty()) {
                                    RecommendShowDialog recommendShowDialog = new RecommendShowDialog(getContext(), recipes);
                                    recommendShowDialog.show();
                                }
                            }

                            @Override
                            public void cancelledCallback(DatabaseError error) {

                            }
                        });
                    }
                    else {
                        Toast.makeText(getContext(), "추천 가능한 레시피가 없습니다.", Toast.LENGTH_SHORT).show();
                    }
                }

                @Override
                public void cancelledCallback(DatabaseError error) {

                }
            });
        }
    });


    public void animateFAB() {

        if (isFabOpen) {

            floatingAddButton.startAnimation(fab_close);
            floatingEditButton.startAnimation(fab_close);
            floatingRecommendButton.startAnimation(fab_close);

            floatingAddButton.setClickable(false);
            floatingEditButton.setClickable(false);
            floatingRecommendButton.setClickable(false);
            isFabOpen = false;
            Log.d("animateFloating", "close");
        } else {
            floatingAddButton.startAnimation(fab_open);
            floatingEditButton.startAnimation(fab_open);
            floatingRecommendButton.startAnimation(fab_open);

            floatingAddButton.setClickable(true);
            floatingEditButton.setClickable(true);
            floatingRecommendButton.setClickable(true);

            isFabOpen = true;
            Log.d("animateFloating", "open");
        }
    }
}