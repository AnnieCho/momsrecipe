package com.project.innovator.momsrecipe.adapter;

import android.graphics.Bitmap;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.project.innovator.momsrecipe.R;
import com.project.innovator.momsrecipe.callback.ImageAdapterListener;
import com.project.innovator.momsrecipe.views.holder.ImageViewHolder;

import java.util.ArrayList;

public class ImageAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder>{

    private ArrayList<Bitmap> mDataset;
    private ImageAdapterListener listener;
    private Boolean isDetail;

    public ImageAdapter(ArrayList<Bitmap> mDataset){
        this.mDataset = mDataset;
        isDetail = true;
    }

    public ImageAdapter(ArrayList<Bitmap> mDataset, ImageAdapterListener listener){
        this.mDataset = mDataset;
        this.listener = listener;
        isDetail = false;
    }
//    public ImageAdapter(ImageAdapterListener listener) { this.listener = listener; }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.view_picture_item, parent, false);
        Log.i("RecipeFragment", parent.getContext().toString());
//        ViewHolder vh = new ViewHolder(v);
        ImageViewHolder vh = new ImageViewHolder(v);
        return vh;
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, final int position) {
        if(position < mDataset.size()){
            Bitmap image = mDataset.get(position);

            ImageViewHolder imageViewHolder = (ImageViewHolder)holder;
            if(isDetail){
                imageViewHolder.getrInputImgBtn().setVisibility(View.INVISIBLE);
                imageViewHolder.getrInputImgBtn().setEnabled(false);
            }
            imageViewHolder.getrInputImgView().setImageBitmap(mDataset.get(position));
            imageViewHolder.getrInputImgBtn().setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    listener.removeImageItem(position);
                }
            });
        }
//        holder.mImage.setImageBitmap(mDataset.get(position));
    }

    @Override
    public int getItemCount() {
        return mDataset.size();
    }

}