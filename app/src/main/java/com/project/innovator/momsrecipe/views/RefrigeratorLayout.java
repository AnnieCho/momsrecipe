package com.project.innovator.momsrecipe.views;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.graphics.Rect;
import android.util.AttributeSet;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.widget.RelativeLayout;
import android.widget.Toast;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DatabaseError;
import com.project.innovator.momsrecipe.FirebaseUtils;
import com.project.innovator.momsrecipe.callback.MaterialViewCreateListener;
import com.project.innovator.momsrecipe.callback.MaterialViewModifyListener;
import com.project.innovator.momsrecipe.callback.MaterialViewListener;
import com.project.innovator.momsrecipe.R;
import com.project.innovator.momsrecipe.models.Material;
import com.project.innovator.momsrecipe.views.dialog.MaterialViewEditDialog;

import java.util.ArrayList;
import java.util.List;

public class RefrigeratorLayout extends RelativeLayout {

    private static String TAG = RefrigeratorLayout.class.getName();
    private static boolean VISIBLE_MODE = false;

    private List<MaterialView> materialViews = new ArrayList<>();
    private MaterialView targetMaterialView = null;
    private Material targetMaterialViewOldMaterial = null;
    private int offsetX, offsetY;
    private List<Integer> resources = null;

    public RefrigeratorLayout(Context context) {
        super(context);
        init();
    }

    public RefrigeratorLayout(Context context, AttributeSet attrs) {
        super(context, attrs);
        init();
    }

    public RefrigeratorLayout(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init();
    }

    private void init() {
        VISIBLE_MODE = false;
        FirebaseUser user = FirebaseAuth.getInstance().getCurrentUser();
        FirebaseUtils.getAllMaterial(user.getUid(), new FirebaseUtils.MaterialCallback() {
            @Override
            public void getMaterialDataCallback(List<Material> materials) {
                if (materials != null && !materials.isEmpty()) {
                    for (Material target : materials) {
                        addView(createMaterialView(target));
                    }
                }
            }

            @Override
            public void cancelledCallback(DatabaseError error) {

            }
        });
    }

    public void editMaterialView() {
        if (!VISIBLE_MODE) {
            MaterialViewEditDialog materialAddDialog
                = new MaterialViewEditDialog(getContext(), R.style.MaterialDialogStyle, materialViewCreateListener, resources);
            materialAddDialog.show();
            materialAddDialog.setTitleText("식재료 추가");
        } else {
            Toast.makeText(getContext(), "편집모드를 종료 후 다시 시도해주세요", Toast.LENGTH_SHORT).show();
        }
    }

    public void editMaterialView(MaterialView materialView) {
        MaterialViewEditDialog materialAddDialog
            = new MaterialViewEditDialog(getContext(), R.style.MaterialDialogStyle, materialViewModifyListener, resources, materialView);
        materialAddDialog.show();
        materialAddDialog.setTitleText("식재료 수정");
    }

    public void setResources(List<Integer> resources) {
        this.resources = resources;
    }

    @Override
    public void addView(View child) {
        if (!VISIBLE_MODE) {
            materialViews.add((MaterialView) child);
            super.addView(child);
        }
    }

    @Override
    public void removeView(View view) {
        materialViews.remove(view);
        super.removeView(view);
    }

    private void removeAllChildView() {
        materialViews.removeAll(materialViews);
        removeAllViews();
    }

    @Override
    public boolean onTouchEvent(MotionEvent event) {
        final int x = (int) event.getX();
        final int y = (int) event.getY();

        switch (event.getAction() & MotionEvent.ACTION_MASK) {
            case MotionEvent.ACTION_DOWN:
                resetData();
                setData(x, y);
                return true;
            case MotionEvent.ACTION_MOVE:
                setMaterialViewPosition(x, y);
                return true;
            case MotionEvent.ACTION_UP:
                updateMaterialViewPosition();
                resetData();
                return false;
        }
        return super.onTouchEvent(event);
    }

    private MaterialView getTouchMaterialView(int x, int y) {
        for (MaterialView target : materialViews) {
            if (target.getBounds().contains(x, y))
                return target;
        }
        return null;
    }

    private void switchMaterialViewVisibleState() {
        if (materialViews != null && !materialViews.isEmpty()) {
            for (MaterialView target : materialViews) {
                target.switchVisibleState(VISIBLE_MODE);
            }
        }
    }

    public boolean switchMaterialEditMode() {
        switchMaterialViewVisibleState();
        VISIBLE_MODE = !VISIBLE_MODE;
        if (VISIBLE_MODE) {
            Toast.makeText(getContext(), "편집모드로 전환 되었습니다.", Toast.LENGTH_SHORT).show();
        } else {
            Toast.makeText(getContext(), "편집모드가 종료 되었습니다.", Toast.LENGTH_SHORT).show();
        }
        return VISIBLE_MODE;
    }

    public boolean getVisibleMode() {
        return VISIBLE_MODE;
    }

    private void setData(int x, int y) {
        targetMaterialView = getTouchMaterialView(x, y);
        if (targetMaterialView != null) {
            targetMaterialViewOldMaterial = targetMaterialView.getMaterial().copy();
            Rect bounds = targetMaterialView.getBounds();
            offsetX = x - bounds.left;
            offsetY = y - bounds.top;
            targetMaterialView.bringToFront();
        }
    }

    private void resetData() {
        targetMaterialView = null;
        targetMaterialViewOldMaterial = null;
        offsetX = 0;
        offsetY = 0;
    }


    private void setMaterialViewPosition(int x, int y) {
        if (targetMaterialView != null) {
            targetMaterialView.setPxPosition(x - offsetX, y - offsetY);
        }
    }

    private void updateMaterialViewPosition() {
        if (targetMaterialView != null) {
            FirebaseUser user = FirebaseAuth.getInstance().getCurrentUser();
            FirebaseUtils.updateMaterialData(user.getUid(), targetMaterialViewOldMaterial, targetMaterialView.getMaterial());
        }
    }

    private MaterialView createMaterialView(Material material) {
        MaterialView materialView = new MaterialView(getContext(), material);
        materialView.setPxPosition(material.getConvertDpToPxXPosition(getContext()), material.getConvertDpToPxYPosition(getContext()));
        materialView.setMaterialViewEditListener(new MaterialViewListener() {
            @Override
            public void removeView(final MaterialView materialView) {
                AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(getContext());
                alertDialogBuilder.setTitle("식재료 삭제");
                alertDialogBuilder
                    .setMessage("'" + materialView.getMaterial().getName() + "'을 삭제하시겠습니까?")
                    .setCancelable(false)
                    .setNegativeButton("예", new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int id) {
                            FirebaseUser user = FirebaseAuth.getInstance().getCurrentUser();
                            FirebaseUtils.removeMaterialData(user.getUid(), materialView.getMaterial());
                            RefrigeratorLayout.this.removeView(materialView);
                        }
                    })
                    .setPositiveButton("아니요", new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int id) {
                            dialog.cancel();
                        }
                    });

                AlertDialog alertDialog = alertDialogBuilder.create();
                alertDialog.show();
            }

            @Override
            public void editView(MaterialView materialView) {
                editMaterialView(materialView);
            }
        });
        return materialView;
    }


    private MaterialViewCreateListener materialViewCreateListener = new MaterialViewCreateListener() {
        @Override
        public void createView(Material material) {
            material.setConvertPxToDpXPosition(getContext(), 200f);
            material.setConvertPxToDpYPosition(getContext(), 200f);
            FirebaseUser user = FirebaseAuth.getInstance().getCurrentUser();
            String key = FirebaseUtils.getMaterialAutoKey(user.getUid());
            FirebaseUtils.addMaterialData(user.getUid(), key, material);
            addView(createMaterialView(material));
//            removeAllChildView();
//            init();
        }
    };

    private MaterialViewModifyListener materialViewModifyListener = new MaterialViewModifyListener() {
        @Override
        public void modifyView(MaterialView materialView, Material newMaterial) {
            Log.i(TAG, "view x : " + materialView.getMaterial().getDpXPosition());
            Log.i(TAG, "view y : " + materialView.getMaterial().getDpYPosition());
            Log.i(TAG, "x : " + newMaterial.getDpXPosition());
            Log.i(TAG, "y : " + newMaterial.getDpYPosition());
            FirebaseUser user = FirebaseAuth.getInstance().getCurrentUser();
            FirebaseUtils.updateMaterialData(user.getUid(), materialView.getMaterial(), newMaterial);
            materialView.setMaterial(newMaterial);
            materialView.insertDataViews();
        }
    };

}