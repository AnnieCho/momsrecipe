package com.project.innovator.momsrecipe.models;

import android.content.Context;
import android.content.res.Resources;
import android.util.DisplayMetrics;

import java.util.HashMap;
import java.util.Map;

public class Material {
    private String name;
    private String amount;
    private int resource;
    private float xPosition,
                  yPosition;


    public Material() {

    }

    public Material(String name, String amount, int resource) {
        this.name = name;
        this.amount = amount;
        this.resource = resource;
    }

    public Material(String name, String amount, int resource, float xPosition, float yPosition) {
        this.name = name;
        this.amount = amount;
        this.resource = resource;
        this.xPosition = xPosition;
        this.yPosition = yPosition;
    }

    public void setName(String name ) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public void setAmount(String amount) {
        this.amount = amount;
    }

    public String getAmount() {
        return amount;
    }

    public void setResource(int resource) {
        this.resource = resource;
    }

    public int getResource() {
        return resource;
    }

    public void setConvertPxToDpXPosition(Context context, float xPosition) {
        this.xPosition = convertPixelsToDp(context, xPosition);
    }

    public void setDpXPosition(float xPosition) {
        this.xPosition = xPosition;
    }

    public float getDpXPosition() {
        return xPosition;
    }

    public float getConvertDpToPxXPosition(Context context) {
        return convertDpToPixel(context, xPosition);
    }

    public void setConvertPxToDpYPosition(Context context, float yPosition) {
        this.yPosition = convertPixelsToDp(context, yPosition);
    }

    public void setDpYPosition(float yPosition) {
        this.yPosition = yPosition;
    }

    public float getDpYPosition() {
        return yPosition;
    }

    public float getConvertDpToPxYPosition(Context context) {
        return convertDpToPixel(context, yPosition);
    }

    public Map<String, Object> toMap() {
        HashMap<String, Object> result = new HashMap<>();
        result.put("name", name);
        result.put("amount", amount);
        result.put("resource", resource);
        result.put("xPosition", xPosition);
        result.put("yPosition", yPosition);
        return result;
    }

    public boolean equals(Material material) {
        if(this.name.equals(material.getName())
            && this.amount.equals(material.getAmount())
            && this.resource == material.getResource()
            && this.xPosition == material.getDpXPosition()
            && this.yPosition == material.getDpYPosition())
            return true;

        return false;
    }

    public Material copy() {
        return new Material(name, amount, resource, xPosition, yPosition);
    }

    private static float convertDpToPixel(Context context, float dp){
        Resources resources = context.getResources();
        DisplayMetrics metrics = resources.getDisplayMetrics();
        float px = dp * ((float)metrics.densityDpi / DisplayMetrics.DENSITY_DEFAULT);
        return px;
    }

    private static float convertPixelsToDp(Context context, float px){
        Resources resources = context.getResources();
        DisplayMetrics metrics = resources.getDisplayMetrics();
        float dp = px / ((float)metrics.densityDpi / DisplayMetrics.DENSITY_DEFAULT);
        return dp;
    }

}
