package com.project.innovator.momsrecipe.controller;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.TextView;

import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;
import com.project.innovator.momsrecipe.FirebaseUtils;
import com.project.innovator.momsrecipe.R;
import com.project.innovator.momsrecipe.adapter.RecipeAdapter;
import com.project.innovator.momsrecipe.callback.RecipeAdapterListener;
import com.project.innovator.momsrecipe.models.Recipe;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

@SuppressLint("ValidFragment")
public class RecipeFragment extends BaseFragment {

    public static Activity recipeFragmentActivity;
    private ImageButton searchBtn;
    private EditText searchEdt;
    private TextView resultTextView;
    private RecyclerView rRecyclerView;
    private RecyclerView.LayoutManager rLayoutManager;
    private RecipeAdapter rAdapter;
    private FirebaseUser user;
    public static Context context;
    private List<Recipe> recipes;

    public RecipeFragment(int page, String title) {
        super(page, title);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_recipe, null);

        context = getActivity();
        searchEdt = (EditText) view.findViewById(R.id.searchEdt);
        searchEdt.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                String text = charSequence.toString();
                List<Recipe> list = new ArrayList<>();
                if (text.isEmpty()) {
                    for (Recipe recipe : recipes) {
                        list.add(recipe);
                    }
                }
                else {
                    list = getTargetRecipe(text);
                }
                if(rAdapter != null && list != null && !list.isEmpty()) {
                    resultTextView.setVisibility(View.INVISIBLE);
                    rRecyclerView.setVisibility(View.VISIBLE);
                    rAdapter.setRecipes(list);
                }
                else if (list != null && list.isEmpty()) {
                    resultTextView.setVisibility(View.VISIBLE);
                    rRecyclerView.setVisibility(View.INVISIBLE);
                    resultTextView.setText("검색된 결과가 없습니다.");
                }
            }

            @Override
            public void afterTextChanged(Editable editable) {

            }
        });

        searchBtn = view.findViewById(R.id.searchBtn);
        searchBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (searchEdt != null) {
                    String text = searchEdt.getText().toString();
                    List<Recipe> list = new ArrayList<>();
                    if (text.isEmpty()) {
                        for (Recipe recipe : recipes) {
                            list.add(recipe);
                        }
                    }
                    else {
                        list = getTargetRecipe(text);
                    }
                    if(rAdapter != null && list != null && !list.isEmpty()) {
                        resultTextView.setVisibility(View.INVISIBLE);
                        rRecyclerView.setVisibility(View.VISIBLE);
                        rAdapter.setRecipes(list);
                    }
                    else if (list != null && list.isEmpty()) {
                        resultTextView.setVisibility(View.VISIBLE);
                        rRecyclerView.setVisibility(View.INVISIBLE);
                        resultTextView.setText("검색된 결과가 없습니다.");
                    }
                }
            }
        });

        resultTextView = view.findViewById(R.id.resultTextView);


        recipeFragmentActivity = getActivity();

        rRecyclerView = view.findViewById(R.id.rRecyclerView);
        rRecyclerView.setHasFixedSize(true);
        rLayoutManager = new LinearLayoutManager(getActivity(), LinearLayoutManager.VERTICAL, false);
        rRecyclerView.setLayoutManager(rLayoutManager);

        rAdapter = new RecipeAdapter(recipeAdapterListener);
        rRecyclerView.setAdapter(rAdapter);

        user = FirebaseAuth.getInstance().getCurrentUser();
        FirebaseUtils.getAllRecipe(user.getUid(), new FirebaseUtils.RecipeCallback() {
            @Override
            public void getRecipeDataCallback(List<Recipe> recipes) {
                if (recipes != null) {
                    RecipeFragment.this.recipes = recipes;
                    rAdapter.setRecipes(recipes);
                }
            }

            @Override
            public void cancelledCallback(DatabaseError error) {

            }
        });
        return view;
    }

    private RecipeAdapterListener recipeAdapterListener = new RecipeAdapterListener() {
        @Override
        public void addRecipeItem() {
            Intent intent = new Intent(getActivity(), RecipeInputActivity.class);
            intent.putExtra("isModify", false);
            startActivity(intent);
        }

        @Override
        public void modifyRecipeItem(Recipe recipe) {
            Log.i("RecipeFragment", recipe.getName());
            Intent intent = new Intent(getActivity(), RecipeInputActivity.class);
            intent.putExtra("recipe", recipe);
            intent.putExtra("isModify", true);
            Log.i("RecipeFragment", "putExtra 완료");
            startActivity(intent);
        }

        @Override
        public void removeRecipeItem(Recipe recipe) {
            FirebaseUtils.removeRecipeData(user.getUid(), recipe);
            FirebaseStorage storage = FirebaseStorage.getInstance();
            StorageReference storageRef = storage.getReference();
            HashMap<String, String> images = recipe.getImages();
            for(int i=0; i<images.size(); i++) {
                StorageReference imageRef = storageRef.child(images.get("image" + (i+1)));
                imageRef.delete().addOnSuccessListener(new OnSuccessListener<Void>() {
                    @Override
                    public void onSuccess(Void aVoid) {

                    }
                }).addOnFailureListener(new OnFailureListener() {
                    @Override
                    public void onFailure(@NonNull Exception e) {

                    }
                });
            }
        }

        @Override
        public void clickRecipeItem(Recipe recipe) {
//            Log.i("RecipeFragment", recipe.getName());
//            Log.i("RecipeFragment", recipe.getContents());
            Intent intent = new Intent(getActivity(), RecipeDetailActivity.class);
            intent.putExtra("recipe", recipe);
            startActivity(intent);
        }
    };

    private List<Recipe> getTargetRecipe(String targetString) {
        if (recipes != null && !targetString.isEmpty()) {
            List<Recipe> showRecipes = new ArrayList<>();
            for (Recipe target : recipes) {
                if (target.getIngredients().contains(targetString) ||
                    target.getName().contains(targetString)) {
                    showRecipes.add(target);
                }
            }
            if (rAdapter != null) {
                return showRecipes;
            }
        }
        return null;
    }
}
