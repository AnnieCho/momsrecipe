package com.project.innovator.momsrecipe.controller;

import android.graphics.Bitmap;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.widget.ImageButton;
import android.widget.TextView;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DatabaseError;
import com.project.innovator.momsrecipe.FirebaseUtils;
import com.project.innovator.momsrecipe.R;
import com.project.innovator.momsrecipe.adapter.ImageAdapter;
import com.project.innovator.momsrecipe.models.Recipe;
import com.project.innovator.momsrecipe.models.Standard;
import com.project.innovator.momsrecipe.views.dialog.StandardShowDialog;

import java.util.ArrayList;
import java.util.List;

public class RecipeDetailActivity extends AppCompatActivity {

    private ImageButton rDetailStandard;
    private TextView rDetailName, rDetailIngredient, rDetailCondiment, rDetailContent;
    private RecyclerView mRecyclerView;
    private RecyclerView.LayoutManager mLayoutManager;
    private RecyclerView.Adapter mAdapter;
    private ArrayList<Bitmap> mDataset;

    @Override
    protected void onCreate(final Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_recipe_detail);

        Recipe recipe = (Recipe)getIntent().getParcelableExtra("recipe");
        Log.i("RecipeFragment", recipe.getName());
        Log.i("RecipeFragment", recipe.getImages().get("image1"));

        mDataset = new ArrayList<>();

        rDetailName = (TextView)findViewById(R.id.rDetailName);
        rDetailName.setText(recipe.getName());
        rDetailIngredient = (TextView)findViewById(R.id.rDetailIngredient);
        rDetailIngredient.setText(recipe.getIngredients());
        rDetailCondiment = (TextView)findViewById(R.id.rDetailCondiment);
        rDetailCondiment.setText(recipe.getCondiment());
        rDetailContent = (TextView)findViewById(R.id.rDetailContent);
        rDetailContent.setText(recipe.getContents());

        mRecyclerView = findViewById(R.id.rDetailRecyclerView);
        mRecyclerView.setHasFixedSize(true);
        mLayoutManager = new LinearLayoutManager(this, LinearLayoutManager.HORIZONTAL, false);
        mRecyclerView.setLayoutManager(mLayoutManager);
        mAdapter = new ImageAdapter(mDataset);


        rDetailStandard = findViewById(R.id.rDetailStandard);
        rDetailStandard.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                FirebaseUser user = FirebaseAuth.getInstance().getCurrentUser();
                FirebaseUtils.getAllStandard(user.getUid(), new FirebaseUtils.StandardCallback() {
                    @Override
                    public void getStandardDataCallback(List<Standard> standards) {
                        if(standards != null && !standards.isEmpty()) {
                            StandardShowDialog standardShowDialog = new StandardShowDialog(RecipeDetailActivity.this, standards);
                            standardShowDialog.show();
                        }
                    }
                    @Override
                    public void cancelledCallback(DatabaseError error) {

                    }
                });
            }
        });

        for (int i = 0; i < recipe.getImages().size(); i++) {
            FirebaseUtils.getTargetBitmap(recipe.getImages().get("image" + (i + 1)), new FirebaseUtils.ImageCallback() {
                @Override
                public void getImageDataCallback(boolean success, Bitmap bitmap) {
                    if (success == true) {
                        Log.i("RecipeFragment", "bitmap가져옴");
                        mDataset.add(bitmap);
                        mRecyclerView.setAdapter(mAdapter);
                        mAdapter.notifyDataSetChanged();
                    } else {
                        Log.i("RecipeFragment", "bitmap 못가져옴");
                    }
                }
            });
        }
    }
}
