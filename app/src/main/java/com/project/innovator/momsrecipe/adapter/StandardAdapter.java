package com.project.innovator.momsrecipe.adapter;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.graphics.Bitmap;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.project.innovator.momsrecipe.FirebaseUtils;
import com.project.innovator.momsrecipe.R;
import com.project.innovator.momsrecipe.callback.StandardAdapterListener;
import com.project.innovator.momsrecipe.models.Standard;
import com.project.innovator.momsrecipe.views.holder.StandardFooterViewHolder;
import com.project.innovator.momsrecipe.views.holder.StandardViewHolder;

import java.util.List;

//item 정의
public class StandardAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private static final int ITEM = 1;
    private static final int FOOTER = 2;
    private static final int FOOTERSIZE = 1;

    private Context context;
    private LayoutInflater layoutInflater;
    //아이템 리스트
    private List<Standard> standards;
    private StandardAdapterListener listener;

//    private final OnClickListener mOnClickListener = new MyOnClickListener();

    public StandardAdapter(Context context, StandardAdapterListener listener) {
        this.context = context;
        this.listener = listener;

        //부분 화면 위해 메모리에 객체화 하기위해 인플레이션 객체 필요
        layoutInflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    @NonNull
    @Override
    //onCreateViewHolder로 viewholder생성 view타입당 1개씩 생성
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        //view 표현
        if (viewType == ITEM) {
            View view = layoutInflater.inflate(R.layout.view_standard_item, parent, false);
            return new StandardViewHolder(view);
        } else if (viewType == FOOTER) {
            View view = layoutInflater.inflate(R.layout.view_standard_footer, parent, false);
            return new StandardFooterViewHolder(view);
        }
        return null;
    }

    //holer이 View데이터 노출 정의
    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, final int position) {
        if (holder instanceof StandardViewHolder) {
            if (position < standards.size()) {
                final Standard standard = standards.get(position);
                final StandardViewHolder standardViewHolder = (StandardViewHolder) holder;
                //data 노출
                standardViewHolder.getStandardNameText().setText(standard.getName());

                if(standard.getImage() !=null){
                    standardViewHolder.getStandardImage().setImageResource(R.drawable.standard_image);
                    FirebaseUtils.getTargetBitmap(standard.getImage(), new FirebaseUtils.ImageCallback() {
                        @Override
                        public void getImageDataCallback(boolean success, Bitmap bitmap) {

                            if (success == true) {
                                standardViewHolder.getStandardImage().setImageBitmap(bitmap);
                            }
//                          else {
//                                standardViewHolder.getStandardImage().setImageResource(R.drawable.standard_image);
//                            }
                        }
                    });
                }

                standardViewHolder.getDeleteButton().setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(view.getContext());
                        alertDialogBuilder.setTitle("기준 삭제");
                        alertDialogBuilder.setMessage("'" + standard.getName() + "'를 삭제하시겠습니까?")
                            .setCancelable(false)
                            .setNegativeButton("예", new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialogInterface, int i) {
                                    listener.removeStandard(standard);
                                    standards.remove(position);
                                    notifyItemRemoved(position);
                                    notifyDataSetChanged();
                                }
                            })
                            .setPositiveButton("아니요", new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialogInterface, int i) {
                                    dialogInterface.cancel();
                                }
                            });
                        AlertDialog alertDialog = alertDialogBuilder.create();
                        alertDialog.show();
                    }
                });

            }
        } else if (holder instanceof StandardFooterViewHolder) {
            StandardFooterViewHolder standardFooterViewHolder = (StandardFooterViewHolder) holder;
            standardFooterViewHolder.addButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    listener.addStandard();
                }
            });
        }

    }


    @Override
    public int getItemCount() {
        if (standards != null)
            return standards.size() + FOOTERSIZE;
        else
            return FOOTERSIZE;
    }

    @Override
    public int getItemViewType(int position) {
        if (checkFooterPosition(position))
            return FOOTER;
        else
            return ITEM;
    }

    private boolean checkFooterPosition(int position) {
        if (standards != null)
            return position == standards.size();
        else
            return true;
    }

    public void setStandards(List<Standard> standards) {
//        Log.d("standardAdapter",standards);
        this.standards = standards;
        notifyDataSetChanged();
    }
}