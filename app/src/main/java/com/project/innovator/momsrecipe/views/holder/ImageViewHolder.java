package com.project.innovator.momsrecipe.views.holder;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageButton;
import android.widget.ImageView;

import com.project.innovator.momsrecipe.R;

public class ImageViewHolder extends RecyclerView.ViewHolder{

    private ImageView rInputImgView;
    private ImageButton rInputImgBtn;

    public ImageViewHolder(View itemView){
        super(itemView);
        rInputImgView = (ImageView)itemView.findViewById(R.id.rInputImgView);
        rInputImgBtn = (ImageButton)itemView.findViewById(R.id.rInputImgBtn);
    }

    public ImageView getrInputImgView() {
        return rInputImgView;
    }

    public ImageButton getrInputImgBtn() {
        return rInputImgBtn;
    }
}
