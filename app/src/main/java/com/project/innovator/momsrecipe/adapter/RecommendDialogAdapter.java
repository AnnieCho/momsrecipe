package com.project.innovator.momsrecipe.adapter;

import android.content.Context;
import android.graphics.Bitmap;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.project.innovator.momsrecipe.FirebaseUtils;
import com.project.innovator.momsrecipe.R;
import com.project.innovator.momsrecipe.models.Recipe;
import com.project.innovator.momsrecipe.views.holder.RecommendDialogViewHolder;

import java.util.List;

public class RecommendDialogAdapter extends RecyclerView.Adapter {

    private Context context;
    private LayoutInflater layoutInflater;
    private List<Recipe> items;


    public RecommendDialogAdapter(Context context, List<Recipe> items) {
        this.context = context;
        this.items = items;

        layoutInflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = layoutInflater.inflate(R.layout.dialog_recommend_item, parent, false);
        return new RecommendDialogViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull final RecyclerView.ViewHolder holder, int position) {
        if(items != null) {
            ((RecommendDialogViewHolder)holder).getTitleImageView().setImageResource(R.drawable.cooking_image);
            FirebaseUtils.getTargetBitmap(items.get(position).getImages().get("image1"), new FirebaseUtils.ImageCallback() {
                @Override
                public void getImageDataCallback(boolean success, Bitmap bitmap) {
                    if(success)
                        ((RecommendDialogViewHolder)holder).getTitleImageView().setImageBitmap(bitmap);
                    else {
                        ((RecommendDialogViewHolder)holder).getTitleImageView().setImageResource(R.drawable.cooking_image);
                    }
                }
            });
            ((RecommendDialogViewHolder) holder).getNameTextView().setText(items.get(position).getName());
        }
    }

    @Override
    public int getItemCount() {
        if(items != null)
            return items.size();
        else
            return 0;
    }
}
