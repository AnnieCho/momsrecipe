package com.project.innovator.momsrecipe.views.holder;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;

import com.project.innovator.momsrecipe.R;

public class RecipeViewHolder extends RecyclerView.ViewHolder{

    private ImageView rListImage;
    private TextView rListName;
    private TextView rListIngredient;
    private TextView rListCondiment;

    private ImageButton rListModify;
    private ImageButton rListDelete;

    public RecipeViewHolder(View itemView){
        super(itemView);
        rListImage = (ImageView)itemView.findViewById(R.id.rListImage);
        rListName = (TextView)itemView.findViewById(R.id.rListName);
        rListIngredient = (TextView)itemView.findViewById(R.id.rListIngredient);
        rListCondiment = (TextView)itemView.findViewById(R.id.rListCondiment);

        rListModify = (ImageButton)itemView.findViewById(R.id.rListModify);
        rListDelete = (ImageButton)itemView.findViewById(R.id.rListDelete);
    }

    public ImageView getListImage() { return rListImage; }
    public TextView getListName() { return rListName; }
    public TextView getListIngredient() { return rListIngredient; }
    public TextView getListCondiment() { return rListCondiment; }

    public ImageButton getListModify() { return rListModify; }
    public ImageButton getListDelete() { return rListDelete; }
}